#include <iostream>
#include <cfloat>
#include <vector>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <omp.h>
#include <mpi.h>
#include <fstream>

/**
 * POPIS FUNKČNOSTI:
 * Process 0, thread 0 rozdělí ukoly do vektoru Tasks, poté rozesílá tyto ukoly ostatním procesům.
 * Process 0, thread != 0 si sám bere úkoly z vektoru Tasks (vezme si jeden) a začne ho vykonávat funkcí Find, ve které se pomocí OpenMP rozvětví přes příkaz "task"
 * Process != 0 dostane úkol a řeší ho stejně jako process 0. Poté odešle výsledek zpět.
 * Pro komunikaci používám MPI_Sendrecv, protože slave posílá výsledek (min_HR a množinu bodů) a master posílá dosud nejmenší HR pro lepší B&B (branch and bound).
 */



/// slave->master -  ready to begin work
#define TAG_START 1
/// master->slave - sending work
#define TAG_WORK 2
/// slave->master - work is done
#define TAG_DONE 3
/// master->slave - no more work - good night
#define TAG_END 4

#define PROC_MASTER 0
uint32_t THREAD_NUM = 1;

using namespace std;
struct Hrana {
    Hrana(uint8_t vrchol, float delka) : vrchol(vrchol), delka(delka) {}
    uint8_t vrchol;
    float delka;
};

//GLOBAL VARS
uint16_t n, k, a, na;
vector<vector<Hrana>> graf;
uint16_t mpi_index = 5;
uint16_t omp_index = 10;

struct Solution{
    float min_HR = FLT_MAX;
    uint64_t min_vector = 0;
};
Solution best;

struct Task{
    Task(uint64_t konfig, float hr, uint8_t numA) : konfig(konfig), HR(hr), num_a(numA) { best_HR = FLT_MAX; }
    Task() = default;

    float best_HR;
    uint64_t konfig;
    float HR;
    uint8_t num_a;
};


MPI_Datatype task_type;
MPI_Datatype solution_type;

/**
 * Řešení úkolů paralelně.
 * @param konfig vektor, kde jsou již nastaveny 1/0
 * @param index index který právě zpracovám (již je nastavený na aktuální)
 * @param HR aktuální hradlový řez (validní -> zkontrolovaný v předchozí iteraci)
 * @param num_a počet jedniček ve vektoru
 *
 */
void find(uint64_t konfig, uint8_t index, float HR, uint8_t num_a){

    if( HR > best.min_HR ) {
        return;
    }
    if(index == n){
        if( HR < best.min_HR ) {
            #pragma omp critical (HR)
            {
                if( HR < best.min_HR ) {
                    best.min_HR = HR;
                    best.min_vector = konfig;
                }
            }
        }
        return;
    }

    //spočtu rozdíl HR po přidání do dané množiny (najendou)
    float pridani_0 = 0, pridani_1 = 0;
    auto sousedi = graf[index].begin();
    auto sousedi_end = graf[index].end();

    for (; sousedi < sousedi_end; sousedi++) {
        if(konfig & ( (uint64_t)1 << sousedi->vrchol) )         //v jaké je množině?
            pridani_0 += sousedi->delka;
        else
            pridani_1 += sousedi->delka;
    }

    //přidávání
    uint16_t inum_a = index - num_a;
    bool mala_jednicka = num_a < a;
    bool velka_jednicka = num_a < na;
    bool mala_nula = inum_a < a;
    bool velka_nula = inum_a < na;
    bool mala_jednicka_r = num_a <= a;
    //bool velka_jednicka_r = num_a <= na;
    bool mala_nula_r = inum_a <= a;
    //bool velka_nula_r = inum_a <= na;

    if(mala_jednicka || (mala_nula_r && velka_jednicka) ){
        uint64_t konfigNew = konfig ^ (uint64_t)1 << index;

        #pragma omp task if(omp_index > index)
        find(konfigNew, index+1, HR+pridani_1, num_a+1);

    }

    if(mala_nula || (mala_jednicka_r && velka_nula)){

        find(konfig, index+1, HR+pridani_0, num_a);
    }


}

vector<Task> Tasks;
/**
 * Rozdělení práce a vložení do vektoru prací.
 * @param konfig vektor, kde jsou již nastaveny 1/0
 * @param index index který právě zpracovám (již je nastavený na aktuální)
 * @param HR aktuální hradlový řez (validní -> zkontrolovaný v předchozí iteraci)
 * @param num_a počet jedniček ve vektoru
 *
 */
void find_work(uint64_t konfig, uint8_t index, float HR, uint8_t num_a){

    if(mpi_index == index){
        Tasks.emplace_back(konfig, HR, num_a);
        return;
    }

    //spočtu rozdíl HR po přidání do dané množiny (najendou)
    float pridani_0 = 0, pridani_1 = 0;
    auto sousedi = graf[index].begin();
    auto sousedi_end = graf[index].end();

    for (; sousedi < sousedi_end; sousedi++) {
        if(konfig & ( (uint64_t)1 << sousedi->vrchol) )         //v jaké je množině?
            pridani_0 += sousedi->delka;
        else
            pridani_1 += sousedi->delka;
    }

    //zanorim se do dalsi urovně
    uint64_t konfigNew = konfig ^ ((uint64_t)1 << index);
    find_work(konfigNew, index+1, HR+pridani_1, num_a+1);

    find_work(konfig, index+1, HR+pridani_0, num_a);


}

/**
 * Master process s ID==0, posílá práci ostatním procesům, sám nic neřeší.
 */
void master(){
    MPI_Status status;
    Solution rcv;
    Task task;
    int num_procs;
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

    while(true){

        int tflag = true;
        #pragma omp critical (task)
        {
            if(Tasks.size()==0){
                tflag = false;
            }else{
                task = Tasks.back();
                Tasks.pop_back();
                task.best_HR = best.min_HR;
            }
        }
        if(!tflag) break;


        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Sendrecv(&task, 1, task_type, status.MPI_SOURCE, TAG_WORK, &rcv, 1, solution_type, status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if(status.MPI_TAG == TAG_DONE){
            #pragma omp critical (HR)
            {
                if(rcv.min_HR < best.min_HR){
                    best = rcv;
                }
            }

        }

    }

    //printf("konec tasku\n");
    for (int i = 0; i < num_procs-1; ++i) {
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Sendrecv(&task, 1, task_type, status.MPI_SOURCE, TAG_END, &rcv, 1, solution_type, status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if(status.MPI_TAG == TAG_DONE){
            #pragma omp critical (HR)
            {
                if(rcv.min_HR < best.min_HR){
                    best = rcv;
                }
            }

        }
    }
}

/**
 * Process s ID == 0, ale ne master thread, berou si sami z vektoru Tasků, které řeší
 */
void masters_slave(){
    Task task;
    while(true){

        int tflag = true;
        #pragma omp critical (task)
        {
            if(Tasks.size()==0){
                tflag = false;
            }else{
                task = Tasks.back();
                Tasks.pop_back();
            }

        }
        if(!tflag) break;

        find(task.konfig, mpi_index, task.HR, task.num_a);
        #pragma omp taskwait

    }
}

/**
 * Procesy s ID != 0, Od mastera dostanou ukol z vektoru Tasks, který řeší.
 */
void slave(){
    MPI_Status status;
    Task task;
    MPI_Sendrecv(&best, 1, solution_type, PROC_MASTER, TAG_START, &task, 1, task_type, PROC_MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    while(true){

        if(status.MPI_TAG == TAG_END) break;
        //printf("mam task\n");

        find(task.konfig, mpi_index, task.HR, task.num_a);
        #pragma omp taskwait

        MPI_Sendrecv(&best, 1, solution_type, PROC_MASTER, TAG_DONE, &task, 1, task_type, PROC_MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if (task.best_HR < best.min_HR) {
            best.min_HR = task.best_HR;
        }
    }
    //printf("slejv konci\n");
}



int main(int argc, char *argv[]) {

    ifstream myfile;
    myfile.open(argv[2], std::ifstream::in);


    try{
        THREAD_NUM = atoi(argv[1]);
    }catch (...){
        printf("chyba atoi");
    }


    myfile >> n >> k >> a;
    na = n-a;
    graf = vector<vector<Hrana>>(n, vector<Hrana>());

    int i, j;
    float delka;
    while (true) {
        if( ! (myfile >> i >> j >> delka) ) break;
        graf[(uint8_t)j].emplace_back( (uint8_t)i, delka ); //k vyššímu indexu dám menší
    }

    time_t begin = time(NULL);

    //Začátek práce
    MPI_Init(&argc, &argv);
    int proc_num, num_procs;

    MPI_Comm_rank(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

    // Create the datatype - SOLUTION
    int lengths_sol[2] = { 1, 1};
    const MPI_Aint displacements_sol[2] = {offsetof(Solution, min_HR), offsetof(Solution, min_vector)};
    MPI_Datatype types_sol[2] = { MPI_FLOAT , MPI_UINT64_T };
    MPI_Type_create_struct(2, lengths_sol, displacements_sol, types_sol, &solution_type);
    if(MPI_SUCCESS != MPI_Type_commit(&solution_type) ){
        cout << "chyba" << endl;
    }


    // Create the datatype - TASK
    int lengths_task[4] = { 1, 1, 1, 1 };
    const MPI_Aint displacements_task[4] = {offsetof(Task, best_HR), offsetof(Task, konfig), offsetof(Task, HR), offsetof(Task, num_a)};
    MPI_Datatype types_task[4] = { MPI_FLOAT , MPI_UINT64_T, MPI_FLOAT, MPI_INT8_T };
    MPI_Type_create_struct(4, lengths_task, displacements_task, types_task, &task_type);
    if(MPI_SUCCESS != MPI_Type_commit(&task_type) ){
        cout << "chyba" << endl;
    }


    //printf("proc_num: %d\n", proc_num);
    if(proc_num == 0){
        find_work(0, 1, 0, 0);
        //printf("work_end\n");
        #pragma omp parallel num_threads( THREAD_NUM )
        {
            #pragma omp single
            {
                if(num_procs != 1) master();
            }

            #pragma omp single
                masters_slave();
        }

        clock_t end = time(NULL);
        double elapsed_secs = difftime(end, begin);
        cout << best.min_HR << " par "<< argv[2] << " " << num_procs << " " << THREAD_NUM << " " << elapsed_secs << endl;
        //for (int l = 0; l < n; ++l) cout << " " << ((best.min_vector & (uint64_t) 1 << l) ? "1" : "0" ) ;
        //cout << endl << "time: " << elapsed_secs << endl;


    }else{

        #pragma omp parallel num_threads( THREAD_NUM )
        {
            #pragma omp single
                slave();
        }

    }
    //printf("end: %d\n", proc_num);
    MPI_Finalize();
    return 0;

}