cmake_minimum_required(VERSION 3.14)
project(minimalni_hradlovy_rez)

set(CMAKE_CXX_STANDARD 14)

add_executable(minimalni_hradlovy_rez main.cpp)